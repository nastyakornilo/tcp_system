import asyncpg


async def get_pool(host, port, database, user, password):
    return await asyncpg.create_pool(host=host,
                               port=port,
                               database=database,
                               user=user,
                               password=password)


async def save_client(pool, login, status='connected'):
    async with pool.acquire() as conn:
        async with conn.transaction():
            await conn.execute('''
                INSERT INTO clients (login, status) VALUES ($1, $2)
            ''', login, status)
    #logger.debug('New client''s info saved to db')


async def client_exists(pool, login):
    async with pool.acquire() as conn:
        async with conn.transaction():
            return await conn.fetchval('''
            SELECT EXISTS(SELECT * FROM clients where login=$1)
            ''', login)


async def save_message(pool, client_login, message):
    #await save package ot db
    async with pool.acquire() as conn:
        async with conn.transaction():
            await conn.execute('''
                INSERT INTO messages (client_login, text) VALUES ($1, $2)
            ''', client_login, message)
    # logger.debug('Package saved to db')

async def update_client_status(pool, client_login, new_status='disconnected'):
    async with pool.acquire() as conn:
        async with conn.transaction():
            await conn.execute('''
                UPDATE clients SET status=$1 where login=$2
            ''', new_status, client_login)