from configparser import ConfigParser


def get_db_connect_info(config):
    user = config.get('db_connection_info', 'user')
    db = config.get('db_connection_info', 'db')
    host = config.get('db_connection_info', 'host')
    port = config.getint('db_connection_info', 'port')
    password = config.get('db_connection_info', 'password')
    return host, port, db, user, password


def get_server_address(config):
    ip = config.get('server_address','ip')
    port = config.getint('server_address','port')
    return ip, port


def read_config(file_path):
    config = ConfigParser()
    config.read(file_path)
    return config