import asyncio
import asyncpg
from server_config_parser import read_config, \
    get_db_connect_info


CONFIG_FILE = 'server.config'


async def create_tables(host, port, database, user, password):
    conn = await asyncpg.connect(user=user, password=password,
                                  database=database, host=host,
                                 port=port
                                 )
    await conn.execute('''
        CREATE TYPE status AS ENUM ('connected', 'disconnected')
    ''')

    await conn.execute('''
    CREATE TABLE clients(
        -- id serial PRIMARY KEY,
        login text PRIMARY KEY,
        status status NOT NULL
        )
    ''')

    await conn.execute('''
    CREATE TABLE messages(
        id serial PRIMARY KEY,
        client_login text NOT NULL REFERENCES clients (login),
        text text
        )
    ''')


if __name__ == '__main__':
    config = read_config(CONFIG_FILE)
    db_connect_info = get_db_connect_info(config)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(create_tables(*db_connect_info))
    loop.close()
