import asyncio
import uvloop
import logging
from server_config_parser import read_config, \
    get_db_connect_info, get_server_address
import sys
from db_operations import save_client, save_message, \
    get_pool, client_exists, update_client_status


CONFIG_FILE = 'server.config'
TCP_DATA_TYPE = {'status': 0, 'login': 1, 'message': 2}
OK = 0


logging.basicConfig(
    level=logging.DEBUG,
    format='%(name)s: %(message)s',
    stream=sys.stderr,
)
logger = logging.getLogger("server")


class ServerProtocol(asyncio.Protocol):
    def __init__(self, connections_pool):
        self.pool = connections_pool

    def connection_made(self, transport):
        self.transport = transport
        peername = transport.get_extra_info('peername')
        self.logger = logging.getLogger(
            'Peername_{}_{}'.format(*peername)
        )
        self.client_login = None
        self.logger.debug('connection made')

    def data_received(self, data):
        msg_type = data[0]
        # logger.debug('Type of received data: {0}'.format(msg_type))

        message = data[1:].decode()
        # logger.debug('Data received: {0}'.format(message))

        if msg_type == TCP_DATA_TYPE['login']:
            fut = asyncio.ensure_future(self._login_received(message))
        else:
            fut = asyncio.ensure_future(self._message_received(message))
        asyncio.wait_for(fut, 60)

    async def _login_received(self, login):
        self.logger.debug('client with login {0} start messaging'
                          .format(login))
        self.client_login = login
        if await client_exists(self.pool, login):
            await update_client_status(self.pool, login, 'connected')
        else:
            await save_client(self.pool, login)
        self.transport.write(bytes([TCP_DATA_TYPE['status']]))
        self.transport.write(bytes([OK]))

    async def _message_received(self, message):
        self.logger.debug('message ''{0}'' received'.format(message))
        await save_message(self.pool, self.client_login, message)

    def eof_received(self):
        self.logger.debug('received EOF')
        if self.transport.can_write_eof():
            self.transport.write_eof()

    def connection_lost(self, error):
        asyncio.ensure_future(
            update_client_status(self.pool, self.client_login, 'disconnected')
        )
        if error:
            self.logger.error('ERROR: {}'.format(error))
        else:
            self.logger.debug('client {0} disconnected'
                              .format(self.client_login))


def main():
    config = read_config(CONFIG_FILE)
    db_connect_info = get_db_connect_info(config)

    loop = uvloop.new_event_loop()
    asyncio.set_event_loop(loop)

    connections_pool = loop.run_until_complete(
        get_pool(*db_connect_info),
    )

    coro = loop.create_server(lambda: ServerProtocol(connections_pool),
                              *get_server_address(config))
    server = loop.run_until_complete(coro)

    logger.debug('serving on {}'.format(server.sockets[0].getsockname()))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.debug("exit")
    finally:
        server.close()
        loop.close()


if __name__ == "__main__":
    main()