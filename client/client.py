import logging
import asyncio
import time
import sys
from configparser import ConfigParser


TCP_DATA_TYPE = {'status': 0, 'login': 1, 'message': 2}
OK = 0

try:
    CONFIG_FILE = sys.argv[1]
except IndexError:
    CONFIG_FILE = 'client.config'

logging.basicConfig(
    level=logging.DEBUG,
    stream=sys.stderr,
)
logger = logging.getLogger("client")


async def tcp_messaging_client(loop, server_address,
                               login, delay=3, message='Hello'
                               ):

    reader, writer = await asyncio.open_connection(*server_address,
                                                   loop=loop)
    try:
        await send_login(writer, login)
        await reader.readuntil(bytes([OK]))
        await send_text_messages(writer, message, delay)
    except ConnectionResetError:
        logger.debug('The server closed the connection')
        logger.debug('Stop the event loop')
    except KeyboardInterrupt:
        logger.debug('Close the socket')
    finally:
        writer.close()
        loop.stop()


async def _send_data(writer, data_type, data):
    writer.writelines([bytes([data_type]), data.encode()])
    await writer.drain()
    logger.debug('Data sent. Type: {0}. Data: {1}'
                 .format(data_type, data)
    )


async def send_text_messages(writer, message, delay):
    logger.debug('start messaging')
    while True:
        await _send_data(writer,
                         TCP_DATA_TYPE['message'],
                         message)
        time.sleep(delay)


async def send_login(writer, login):
    await _send_data(writer,
                     TCP_DATA_TYPE['login'],
                     login)


def get_client_info(config):
    login = config.get('client_info', 'login')
    delay = config.getint('client_info', 'delay')
    message = config.get('client_info', 'message')
    return login, delay, message


def get_server_address(config):
    ip = config.get('server_address','ip')
    port = config.getint('server_address','port')
    return ip, port


def main():
    config = ConfigParser()
    config.read(CONFIG_FILE)

    login, delay, message = get_client_info(config)
    server_address = get_server_address(config)

    loop = asyncio.get_event_loop()

    loop.run_until_complete(tcp_messaging_client(loop, server_address, login,delay,message))

    # loop.run_forever()
    print('exit')
    loop.close()


if __name__ == '__main__':
    main()