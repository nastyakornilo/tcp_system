#System with TCP-communication

Data format: data type _ data

Possible data types: status, login, message

##Client:

**Description:** read configurations from config file (default:client.config), connect to server, send user login(read from config), 
send message one time per sleeping period;

**Run:**

    python3 client.py [config_file=client.config]

**Config:** [client config example](https://bitbucket.org/AnastasiaKornilo/tcp_system/src/c0422763b08fb8f76c31a3b065f9b2b8053b8538/client/client.config?at=master&fileviewer=file-view-default)

client_info: login, delay (client sleeping time), message

server_address: ip, port

##Server:

**Description:** asynchronous server, read configurations from config file (default:server.config), save to database client's login, status: connected/disconnected,
send message one time per sleeping period;

**Run:**

    pip install -r requirements.txt
	python3 tables_creation.py
    python3 server.py [config_file=server.config]

**Config:** [server config example](https://bitbucket.org/AnastasiaKornilo/tcp_system/src/c0422763b08fb8f76c31a3b065f9b2b8053b8538/server/server.config?at=master&fileviewer=file-view-default)

server_address: ip, port

db_connection_info: user, db, host, port, password



